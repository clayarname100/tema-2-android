package com.example.tema2android.fragments;

import android.app.VoiceInteractor;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tema2android.R;
import com.example.tema2android.adapters.UserAdaptor;
import com.example.tema2android.interfaces.ActivityFragmentCommunication;
import com.example.tema2android.interfaces.OnItemClickListener;
import com.example.tema2android.models.Album;
import com.example.tema2android.models.Post;
import com.example.tema2android.models.User;
import com.example.tema2android.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FirstFragment extends Fragment {

    private ArrayList<User> userList = new ArrayList<User>();
    private ActivityFragmentCommunication activityFragmentCommunication;
    private UserAdaptor userAdaptor = new UserAdaptor(userList, new OnItemClickListener() {
        @Override
        public void onUserClick(User user) {
            if(activityFragmentCommunication != null)
            {
                activityFragmentCommunication.replaceWithSecondFragment(user);
            }
        }

        @Override
        public void onButtonClick(User user) {
            fetchPostsForUser(user);
        }

        @Override
        public void onAlbumClick(Album album) {

        }
    });

    FirstFragment(){
    }

    public static FirstFragment newInstance() {
        FirstFragment fragment = new FirstFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        fetchUsers();
    }

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView usersRecyclerView = (RecyclerView) view.findViewById(R.id.users_list);
        usersRecyclerView.setLayoutManager(linearLayoutManager);
        usersRecyclerView.setAdapter(userAdaptor);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if(context instanceof ActivityFragmentCommunication)
        {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }

    public void fetchUsers(){
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://jsonplaceholder.typicode.com/users";
        StringRequest getUsersRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handleUserResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(getUsersRequest);
                }


    public void handleUserResponse(String response) throws JSONException {
        JSONArray userJsonArray = new JSONArray(response);
        for(int index = 0 ; index < userJsonArray.length() ; index++)
        {
            JSONObject userObject = userJsonArray.getJSONObject(index);
            String username = userObject.getString("username");
            int id = userObject.getInt("id");
            User user = new User(id, username);
            userList.add(user);
        }
        userAdaptor.notifyDataSetChanged();
    }

    public void fetchPostsForUser(User user) {
        VolleyConfigSingleton volleyConfigSingleton = VolleyConfigSingleton.getInstance(getContext());
        RequestQueue queue = volleyConfigSingleton.getRequestQueue();
        String url = "https://jsonplaceholder.typicode.com/users";
        url += "/" + user.getId() + "/posts";
        StringRequest getPostsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handlePostResponse(response, user);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(getPostsRequest);
    }

    public void handlePostResponse(String response, User user) throws JSONException{
        user.getPostList().clear();
        JSONArray postJsonArray = new JSONArray(response);
        for(int index = 0 ; index < postJsonArray.length() ; index++)
        {
            JSONObject postObject = postJsonArray.getJSONObject(index);
            if(postObject!=null)
            {
                int id = postObject.getInt("id");
                String title = postObject.getString("title");
                Post post = new Post(id, title);
                if (!user.getPostList().contains(post))
                {
                    user.getPostList().add(post);
                }
            }
        }
        userAdaptor.notifyDataSetChanged();
    }
}