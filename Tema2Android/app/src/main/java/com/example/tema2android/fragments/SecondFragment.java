package com.example.tema2android.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tema2android.R;
import com.example.tema2android.adapters.AlbumAdaptor;
import com.example.tema2android.interfaces.ActivityFragmentCommunication;
import com.example.tema2android.interfaces.OnItemClickListener;
import com.example.tema2android.models.Album;
import com.example.tema2android.models.User;
import com.example.tema2android.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SecondFragment extends Fragment {

    private ArrayList<Album> albumList = new ArrayList<Album>();
    private ActivityFragmentCommunication activityFragmentCommunication;
    private User user;
    private AlbumAdaptor albumAdaptor = new AlbumAdaptor(albumList, new OnItemClickListener() {
        @Override
        public void onUserClick(User user) {
            
        }

        @Override
        public void onButtonClick(User user) {

        }

        @Override
        public void onAlbumClick(Album album) {
            if(activityFragmentCommunication != null)
            {
               activityFragmentCommunication.replaceWithSecondFragment(user);
            }
        }
    });

    SecondFragment(User user){
        this.user= user;
    }

    public static SecondFragment newInstance(User user) {
        SecondFragment fragment = new SecondFragment(user);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        fetchAlbums();
    }

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        RecyclerView usersRecyclerView = (RecyclerView) view.findViewById(R.id.album_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        usersRecyclerView.setLayoutManager(linearLayoutManager);
        usersRecyclerView.setAdapter(albumAdaptor);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if(context instanceof ActivityFragmentCommunication)
        {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }

    public void fetchAlbums(){
        VolleyConfigSingleton volleyConfigSingleton = VolleyConfigSingleton.getInstance(getContext());
        RequestQueue queue = volleyConfigSingleton.getRequestQueue();
        String url = "https://jsonplaceholder.typicode.com/users/";
        url += user.getId() + "/albums";
        StringRequest getAlbumsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handleAlbumsResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(getAlbumsRequest);
    }


    public void handleAlbumsResponse(String response) throws JSONException {
        JSONArray albumJsonArray = new JSONArray(response);
        for(int index = 0 ; index < albumJsonArray.length() ; index++)
        {
            JSONObject albumObject = albumJsonArray.getJSONObject(index);
            String name = albumObject.getString("name");
            int id = albumObject.getInt("id");
            Album album = new Album(id,name);
            albumList.add(album);
        }
        albumAdaptor.notifyDataSetChanged();
    }
}