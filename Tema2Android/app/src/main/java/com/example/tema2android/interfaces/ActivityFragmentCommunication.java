package com.example.tema2android.interfaces;

import com.example.tema2android.models.Album;
import com.example.tema2android.models.User;

public interface ActivityFragmentCommunication {
    public void replaceWithSecondFragment(User user);
    public void replaceWithThirdFragment(Album album);
}
