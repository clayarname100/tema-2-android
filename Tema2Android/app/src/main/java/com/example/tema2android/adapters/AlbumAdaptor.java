package com.example.tema2android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tema2android.R;
import com.example.tema2android.interfaces.OnItemClickListener;
import com.example.tema2android.models.Album;

import java.util.ArrayList;

public class AlbumAdaptor extends RecyclerView.Adapter<AlbumAdaptor.AlbumViewHolder> {
    private ArrayList<Album> albumList;
    private OnItemClickListener onItemClickListener;
    public AlbumAdaptor(ArrayList<Album> albumList, OnItemClickListener onItemClickListener)
    {
        this.albumList = albumList;
        this.onItemClickListener= onItemClickListener;
    }

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_album, parent ,false);
        AlbumViewHolder albumViewHolder = new AlbumViewHolder(view);
        return albumViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        Album album = albumList.get(position);
        holder.bind(album);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private View view;
        public AlbumViewHolder(View view) {
            super(view);
            this.view= view;
            this.name = view.findViewById(R.id.tv_album_name);

        }

        public void bind(Album album)
        {
            name.setText(album.getName());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onAlbumClick(album);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }


    }

}
