package com.example.tema2android.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tema2android.R
import com.example.tema2android.fragments.FirstFragment
import com.example.tema2android.fragments.SecondFragment
import com.example.tema2android.interfaces.ActivityFragmentCommunication
import com.example.tema2android.models.Album
import com.example.tema2android.models.User

class MainActivity : AppCompatActivity(), ActivityFragmentCommunication {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFirstFragment()
    }

    fun addFirstFragment()
    {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = FirstFragment::class.java.name
        val addTransaction = transaction.add(
            R.id.frame_layout, FirstFragment.newInstance(), tag
        )
        addTransaction.commit()
    }

    override fun replaceWithSecondFragment(user : User)
    {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = SecondFragment::class.java.name
        val replaceTransaction = transaction.replace(
            R.id.frame_layout, SecondFragment.newInstance(user),tag
        )
        replaceTransaction.addToBackStack(tag);
        replaceTransaction.commit()
    }

    override fun replaceWithThirdFragment(album : Album)
    {
       /* val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = SecondFragment::class.java.name
        val replaceTransaction = transaction.replace(
            R.id.frame_layout, SecondFragment.newInstance(album),tag
        )
        replaceTransaction.addToBackStack(tag);
        replaceTransaction.commit()*/
    }
}