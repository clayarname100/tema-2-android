package com.example.tema2android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tema2android.R;
import com.example.tema2android.interfaces.OnItemClickListener;
import com.example.tema2android.models.User;

import java.util.ArrayList;

public class UserAdaptor extends RecyclerView.Adapter<UserAdaptor.UserViewHolder> {
    private ArrayList<User> userList;
    private OnItemClickListener onItemClickListener;
    public UserAdaptor(ArrayList<User> userList, OnItemClickListener onItemClickListener)
    {
        this.userList = userList;
        this.onItemClickListener= onItemClickListener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_user, parent ,false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = userList.get(position);
        holder.bind(user);
        boolean postsVisible = userList.get(position).isPostsVisible();
        holder.postsConstraintLayout.setVisibility(postsVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView username;
        private TextView posts;
        private View view;
        private ImageView button;
        private ConstraintLayout postsConstraintLayout;
        public UserViewHolder(View view) {
            super(view);
            this.view= view;
            this.button = view.findViewById(R.id.iv_button);
            this.postsConstraintLayout = view.findViewById(R.id.posts_window);
            this.username = view.findViewById(R.id.tv_username);
            this.posts = view.findViewById(R.id.tv_posts);
        }

        public void bind(User user)
        {
            username.setText(user.getName());
            String userPosts = "";
            for(int index = 0; index<user.getPostList().size(); index++)
            {
                userPosts += (index+1) +"- " + user.getPostList().get(index).getTitle() + "\n\n" ;
            }
            posts.setText(userPosts);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onUserClick(user);
                }
            });

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    user.setPostsVisible(!user.isPostsVisible());
                    onItemClickListener.onButtonClick(user);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }


    }

}
