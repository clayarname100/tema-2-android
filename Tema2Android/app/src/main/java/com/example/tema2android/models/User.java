package com.example.tema2android.models;

import java.util.ArrayList;

public class User {
    private String name;
    private int id;
    private boolean postsVisible;
    private ArrayList<Post> postList;

    public User(int id, String name) {
        this.name = name;
        this.id = id;
        this.postList = new ArrayList<Post>();
        this.postsVisible=false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPostsVisible() {
        return postsVisible;
    }

    public void setPostsVisible(boolean postsVisible) {
        this.postsVisible = postsVisible;
    }

    public ArrayList<Post> getPostList() {
        return postList;
    }

    public void setPostList(ArrayList<Post> postList) {
        this.postList = postList;
    }
}
