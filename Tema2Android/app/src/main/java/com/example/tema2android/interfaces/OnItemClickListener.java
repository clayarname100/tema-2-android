package com.example.tema2android.interfaces;

import com.example.tema2android.models.Album;
import com.example.tema2android.models.User;

public interface OnItemClickListener {
    public void onUserClick(User user);
    public void onButtonClick(User user);
    public void onAlbumClick(Album album);
}
